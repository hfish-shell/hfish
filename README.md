# hfish

## What it is

This is the main repository of hfish, a reimplementation of the [fish shell][fish-shell] in haskell.
This is not, however, an exact replica of fish (that wouldn't be possible anyway, since to my knowledge there is no formal specification of the fish language).
Refer to the [Syntax](#syntax) section for details on the differences.

## Prerequisites and Dependencies
  * A unix / posix system (linux for example)
  * A recent cabal / ghc
  * Tons of haskell library dependencies (cabal install should take care of them), they are listed in the [cabal file][hfish-cabal-file].


## Building

Currently I do not guarantee that the master branch will build. If you want to try a working executable, build with [nix][nixos-repo] by running:
```sh
nix-build
```

If you want to build directly from master there is a [build script][hfish-build-script].

## Syntax

TODO

## Bugs

Many. A non-exhaustive list can be found in the bug tracker.

## Legal
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.
    
See [LICENSE](LICENSE) for more details.


[hfish-cabal-file]: https://gitlab.com/hfish-shell/hfish/blob/master/hfish.cabal

[hfish-build-script]: https://gitlab.com/hfish-shell/hfish/blob/master/build

[fish-shell]: https://github.com/fish-shell/fish-shell/

[nixos-repo]: https://nixos.org/
