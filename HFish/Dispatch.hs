{-# LANGUAGE TemplateHaskell, GeneralizedNewtypeDeriving #-}
module HFish.Dispatch where

import HFish.Interpreter.Core
import HFish.Types

import Control.Monad.IO.Class
import Control.Monad.State.Strict
import Control.Monad.Catch
import Control.Lens

import qualified Data.Set as S

data DispatchState = DispatchState
  { _dReader :: FishReader
  , _dState :: FishState
  , _dOnError :: Maybe ( HFishError -> Fish () )
  , _dCompat :: FishCompat
  , _dDebug :: S.Set DebugMain }
makeLenses ''DispatchState

newtype Dispatch a = Dispatch
  { getDispatch :: StateT DispatchState IO a }
  deriving ( Functor
           , Applicative
           , Monad
           , MonadIO
           , MonadState DispatchState
           , MonadThrow
           , MonadCatch
           , MonadMask )

evalDispatch :: Dispatch a -> DispatchState -> IO a
evalDispatch (Dispatch m) = evalStateT m



