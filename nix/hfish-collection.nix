# Collection of hfish packages
{ pkgs ? import <nixpkgs> {}
, lib ? pkgs.lib
, haskellPackages ? pkgs.haskell.packages.ghc92
, doProfiling ? false
, doHaddock ? false
}:
let
  fetchGitL = { url ? null, lpath ? null, rev, sha256 ? null, local }:
    if local then builtins.fetchGit { url = lpath; }
    else pkgs.fetchgit { inherit url rev sha256; };

  mkPackage = { name, remote, lpath ? null, local ? false }:
    let src = fetchGitL {
        inherit (remote) url rev sha256;
        inherit lpath;
        inherit local;
      };
    in args: with pkgs.haskell.lib; lib.pipe
      ( haskellPackages.callCabal2nix name src args )
      (
        lib.optionals (!doProfiling)
          [
            disableLibraryProfiling
            disableExecutableProfiling
          ]
        ++ lib.optional (!doHaddock) dontHaddock
        ++ [ doJailbreak ]
      );

  src = import ./hfish-collection-sources.nix;

  hfish = mkPackage {
    name = "hfish";
    remote = src.hfish;
    local = true;
    lpath = ../.;
  } {
    inherit nfc-text posix-fd-io fish-lang
      fish-parser hfish-parser hfish-interpreter
      ;
  };

  hfish-interpreter = mkPackage {
    name = "hfish-interpreter";
    remote = src.hfish-interpreter;
  } {
    inherit nfc-text posix-fd-io fish-lang
      fish-parser hfish-parser
      ;
  };

  hfish-parser = mkPackage {
    name = "hfish-parser";
    remote = src.hfish-parser;
  } { inherit nfc-text fish-lang; };

  fish-parser = mkPackage {
    name = "fish-parser";
    remote = src.fish-parser;
  } { inherit nfc-text fish-lang; };

  fish-lang = mkPackage {
    name = "fish-lang";
    remote = src.fish-lang;
  } { inherit nfc-text; };

  nfc-text = mkPackage {
    name = "nfc-text";
    remote = src.nfc-text;
  } {};

  posix-fd-io = mkPackage {
    name = "posix-fd-io";
    remote = src.posix-fd-io;
  } {};

in {
  inherit
    hfish
    hfish-interpreter
    hfish-parser
    fish-parser
    fish-lang
    posix-fd-io
    nfc-text
  ;
}
