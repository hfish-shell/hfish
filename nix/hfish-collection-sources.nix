# Collection of hfish packages
{
  # Package hashes computed with nix-prefetch-git

  posix-fd-io = {
    url = "https://gitlab.com/hfish-shell/posix-fd-io";
    rev = "ad1dad7dc557948b99d69398e328c5a95c743988";
    sha256 = "029znj2v4nkinl170qjyynjhgnw2jqmn288hp2yddam4i8prmrp9";
  };

  nfc-text = {
    url = "https://gitlab.com/hfish-shell/nfc-text";
    rev = "7797703a47bfef7ae753f8ac9485f44da8e39ca3";
    sha256 = "1g9vba0krks9y6qqajv4ivxp4fjflpxmfzyfcpalq6sk9n3fzahf";
  };

  fish-lang = {
    url = "https://gitlab.com/hfish-shell/fish-lang";
    rev = "cdd563a3bada2fbf5e42571dc85a484904741488";
    sha256 = "03mq7gny3mjfnbbyigxymkrjm174kcr00iwwwf0iypzkb0ffxjgf";
  };

  fish-parser = {
    url = "https://gitlab.com/hfish-shell/fish-parser";
    rev = "6d02b4368f33e7101349c861bfab9879cdef41cc";
    sha256 = "046x7xvwx676g1fif5bw36zd9nza34l4mwmqxmrrk7wl5ypwzhl1";
  };

  hfish-parser = {
    url = "https://gitlab.com/hfish-shell/hfish-parser";
    rev = "a5afbb70ef151d8f0bf25fc1af9cab1037ed7e6d";
    sha256 = "0sv00xhsb27wd1ywjg6x3d9ygjbx7wq1jw5iixrvv8r30wq9cdls";
  };

  hfish-interpreter = {
    url = "https://gitlab.com/hfish-shell/hfish-interpreter";
    rev = "739e3c19448e3a7b82d90d653855949bfc8a5e5c";
    sha256 = "1ixvq09hk4dc8c4zwx4gpgi850z9f3j7flm4diwj9dm156rmrbnw";
  };

}
